package android.os;

/** @hide */
interface IProfiler {

	void enableProfiler();
	void disableProfiler();
	long getInputCount();
	String getForegroundActivity();
	Map getIntervalData();

}