
package android.os;

import android.content.Context;
import android.util.Log;

import java.util.Map;

public class Profiler {
    private static final String TAG = "ProfilerService";
    final Context mContext;
    final IProfiler mService;
    final Handler mHandler;

    public Profiler(Context context, IProfiler service, Handler handler) {
        mContext = context;
        mService = service;
        mHandler = handler;
        Log.i(TAG, "Initialized Profiler via ContextImpl");
    }

    public String getForegroundActivity() throws RemoteException {
        return mService.getForegroundActivity();
    }
    
    public long getInputCount() throws RemoteException {
        return mService.getInputCount();
    }
    
    public Map getProfilerData() {
        try {
            return mService.getIntervalData();
        } catch (RemoteException e) {
            e.printStackTrace();
            Log.i(TAG, "Error in getting IntervalData ");
        }
        return null;
    }

    public void enableProfiler() {
        try {
            mService.enableProfiler();
        } catch (RemoteException e) {
            e.printStackTrace();
            Log.i(TAG, "Error in enabling Profiler ");
        }
    }

    public void disableProfiler() {
        try {
            mService.disableProfiler();
        } catch (RemoteException e) {
            e.printStackTrace();
            Log.i(TAG, "Error in disabling Profiler ");
        }
    }

}
