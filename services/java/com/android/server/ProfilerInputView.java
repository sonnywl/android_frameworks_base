
package com.android.server;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.android.server.am.ActivityManagerService;

public class ProfilerInputView extends View {
    private static final String TAG = "ProfilerInputView";
    private final Paint mPaint;
    private final ActivityManagerService activityManager;
    private String activityForeground;

    public ProfilerInputView(Context c, ActivityManagerService am) {
        super(c);
        activityManager = am;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(13 * getResources().getDisplayMetrics().density);
        mPaint.setColor(Color.WHITE);
        activityForeground = "No Activity";
        Log.i(TAG, "ProfilerInputView Instantiated");
    }

    // Draw an oval. When angle is 0 radians, orients the major axis vertically,
    // angles less than or greater than 0 radians rotate the major axis left or
    // right.
    private RectF mReusableOvalRect = new RectF();

    private void drawOval(Canvas canvas, float x, float y, float major, float minor,
            float angle, Paint paint) {
        canvas.save(Canvas.MATRIX_SAVE_FLAG);
        canvas.rotate((float) (angle * 180 / Math.PI), x, y);
        mReusableOvalRect.left = x - minor / 2;
        mReusableOvalRect.right = x + minor / 2;
        mReusableOvalRect.top = y - major / 2;
        mReusableOvalRect.bottom = y + major / 2;
        canvas.drawOval(mReusableOvalRect, paint);
        canvas.restore();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawText(activityForeground, 10, 20, mPaint);
        // drawOval(canvas, ps.mCoords.x, ps.mCoords.y, ps.mCoords.touchMajor,
        // ps.mCoords.touchMinor, ps.mCoords.orientation, mPaint);

    }

    public void addPointerEvent(MotionEvent event) {
        activityForeground = activityManager.getForegroundActivity();
        invalidate();
    }

}
