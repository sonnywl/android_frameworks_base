
package com.android.server.profiler;

import java.util.HashMap;
import java.util.Set;

/**
 * ProfilerStore
 * 
 * @author sonny
 * @param <T>
 */
public class ProfilerStore<T> {
    HashMap<String, CircularArray<T>> profilerDataSet;
    private static final int SIZE = 6400;

    public ProfilerStore() {
        profilerDataSet = new HashMap<String, CircularArray<T>>();
    }

    public CircularArray<T> get(String key) {
        if (profilerDataSet.containsKey(key)) {
            return profilerDataSet.get(key);
        }
        return new CircularArray<T>(SIZE);
    }

    public void put(String key, T data) {
        if (profilerDataSet.containsKey(key)) {
            CircularArray<T> dataSet = profilerDataSet.get(key);
            dataSet.add(data);
        } else {
            CircularArray<T> array = new CircularArray<T>(SIZE);
            array.add(data);
            profilerDataSet.put(key, array);
        }
    }

    public CircularArray<T> getData(String key) {
        return profilerDataSet.get(key);
    }

    public Set<String> getKeys() {
        return profilerDataSet.keySet();
    }

    public boolean isEmpty() {
        return profilerDataSet.isEmpty();
    }

}
