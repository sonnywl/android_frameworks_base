
package com.android.server.profiler;

import java.util.ArrayList;

/**
 * DataStore is an implementation of a circular buffer
 * 
 * @author sonny
 * @param <T>
 */
public class CircularArray<T> {

    ArrayList<T> dataSet;
    int pointer;
    int size;

    public static void main(String[] args) {
        CircularArray<String> array = new CircularArray<String>(5);
        array.add("1");
        System.out.println(array.getLRU());
        System.out.println(array.getMRU());
    }

    public CircularArray(int capacity) {
        dataSet = new ArrayList<T>(capacity);
        size = capacity;
        pointer = 0;
    }

    public void add(T data) {
        if (dataSet.size() < size) {
            dataSet.add(pointer, data);
        } else {
            dataSet.set(pointer, data);
        }
        increment();
    }

    public T getPrevious() {
        return dataSet.get(getPreviousPosition(1));
    }

    public String getLRU() {
        if (dataSet.size() > 0) {
            StringBuilder sb = new StringBuilder();
            int initpointer = pointer;

            do {
                sb.append(dataSet.get(pointer));
                sb.append(",");
                increment();
            } while (initpointer != pointer);
            return sb.toString();
        }
        return "";
    }

    public String getMRU() {
        if (dataSet.size() > 0) {
            StringBuilder sb = new StringBuilder();
            int initpointer = pointer;
            do {
                decrement();
                sb.append(dataSet.get(pointer));
                sb.append(",");
            } while (initpointer != pointer);

            return sb.toString();
        }
        return "";
    }

    private int getPreviousPosition(int howFarBack) {
        int tmpPointer = pointer;
        tmpPointer -= howFarBack;
        if (tmpPointer < 0 || tmpPointer >= dataSet.size())
            tmpPointer = dataSet.size() - howFarBack;
        return tmpPointer;
    }

    private void increment() {
        pointer++;
        if (pointer >= size || pointer >= dataSet.size())
            pointer = 0;
    }

    private void decrement() {
        pointer--;
        if (pointer < 0 || pointer >= dataSet.size())
            pointer = dataSet.size() - 1;
    }

    public void clear() {
        dataSet.clear();
    }

    public String toString() {
        return getMRU();
    }

}
