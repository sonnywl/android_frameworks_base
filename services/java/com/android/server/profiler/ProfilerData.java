
package com.android.server.profiler;

public class ProfilerData {
    final String dataType;
    int occurrence;

    public ProfilerData(String type, int count) {
        dataType = type;
        occurrence = count;
    }

    public String toString() {
        return dataType + "," + occurrence;
    }
}
