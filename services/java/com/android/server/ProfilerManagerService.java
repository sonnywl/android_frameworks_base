
package com.android.server;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.IProfiler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.view.InputChannel;
import android.view.InputDevice;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.WindowManagerPolicy.WindowManagerFuncs;

import com.android.server.am.ActivityManagerService;
import com.android.server.input.InputManagerService;
import com.android.server.profiler.ProfilerData;
import com.android.server.profiler.ProfilerStore;

import java.util.HashMap;
import java.util.Map;

public class ProfilerManagerService extends IProfiler.Stub
        implements Watchdog.Monitor {
    private static final String TAG = "ProfilerManagerServcie";
    final InputManagerService mInputManager;
    final Context mContext;
    final WindowManagerFuncs mWindowManagerFuncs;
    final ActivityManagerService activityManager;
    final Object mLock = new Object();

    boolean mSystemReady;
    boolean startProfiler;
    SettingsObserver mSettingsObserver;
    Handler mHandler;

    ProfilerInputEventReceiver mProfilerInputEventReceiver;
    ProfilerInputView mProfilerView;
    InputChannel mProfilerInputChannel;
    ProfilerStore<ProfilerData> mStore;

    int mProfilerInputMode = 0; // guarded by mLock

    public ProfilerManagerService(Context context,
            ActivityManagerService am,
            InputManagerService inputManager,
            WindowManagerFuncs windowManagerFuncs) {
        mContext = context;
        mInputManager = inputManager;
        mWindowManagerFuncs = windowManagerFuncs;
        activityManager = am;
        mHandler = new InputHandler();
        mSettingsObserver = new SettingsObserver(mHandler);
        mSettingsObserver.observe();
        mStore = new ProfilerStore<ProfilerData>();
    }

    private void enableProfilerInput() {
        if (mProfilerInputChannel == null) {
            mProfilerInputChannel =
                    mWindowManagerFuncs.monitorInput("ProfilerInputView");
            mProfilerInputEventReceiver =
                    new ProfilerInputEventReceiver(
                            mProfilerInputChannel,
                            Looper.myLooper());
        }
    }

    private void disableProfilerInput() {
        if (mProfilerInputEventReceiver != null) {
            mProfilerInputEventReceiver.clear();
            mProfilerInputEventReceiver.dispose();
            mProfilerInputEventReceiver = null;
        }

        if (mProfilerInputChannel != null) {
            mProfilerInputChannel.dispose();
            mProfilerInputChannel = null;
        }

        if (mProfilerView != null) {
            WindowManager wm = (WindowManager)
                    mContext.getSystemService(Context.WINDOW_SERVICE);
            wm.removeView(mProfilerView);
            mProfilerView = null;
        }

    }

    public void systemReady() {
        synchronized (mLock) {
            mSystemReady = true;
            mHandler.post(new Runnable() {
                public void run() {
                    updateSettings();
                }
            });
        }
    }

    private void updateSettings() {
        ContentResolver resolver = mContext.getContentResolver();
        synchronized (mLock) {
            if (mSystemReady) {
                int profilerInput = Settings.System.getIntForUser(resolver,
                        Settings.System.PROFILER_SERVICE, 0, UserHandle.USER_CURRENT);
                if (mProfilerInputMode != profilerInput) {
                    mProfilerInputMode = profilerInput;
                    mHandler.sendEmptyMessage(mProfilerInputMode != 0 ?
                            MSG_ENABLE_PROFILER : MSG_DISABLE_PROFILER);
                }
            }
        }
    }

    @Override
    public void enableProfiler() throws RemoteException {
        Log.i(TAG, "Enable Profiler via AIDL");
    }

    @Override
    public void disableProfiler() throws RemoteException {
        Log.i(TAG, "Diable Profiler via AIDL");
    }

    @Override
    public Map getIntervalData() throws RemoteException {
        HashMap<String, Integer> copy = new HashMap<String, Integer>();
        copy.putAll(mProfilerInputEventReceiver.getCounts());
        mProfilerInputEventReceiver.clear();
        Log.i(TAG, "Copy of info is " + copy);

        return copy;
    }

    @Override
    public String getForegroundActivity() {
        return activityManager.getForegroundActivity();
    }

    @Override
    public long getInputCount() throws RemoteException {
        if (mProfilerInputEventReceiver != null) {
            long values = mProfilerInputEventReceiver.getInputCounts();
            mProfilerInputEventReceiver.clear();
            return values;
        }
        return 0;

    }

    private static final class ProfilerInputEventReceiver extends InputEventReceiver {
        HashMap<String, Integer> counts;
        long inputCounts;

        public ProfilerInputEventReceiver(
                InputChannel inputChannel,
                Looper looper) {
            super(inputChannel, looper);
            counts = new HashMap<String, Integer>();
        }

        @Override
        public void onInputEvent(InputEvent event) {
            boolean handled = false;
            try {
                if (event instanceof MotionEvent
                        && (event.getSource() & InputDevice.SOURCE_CLASS_POINTER) != 0) {
                    inputCounts++;
                    handled = true;
                }
            } finally {
                finishInputEvent(event, handled);
            }
        }

        public HashMap<String, Integer> getCounts() {
            return counts;
        }

        public long getInputCounts() {
            return inputCounts;
        }

        public void clear() {
            counts.clear();
            inputCounts = 0;
        }
    }

    class SettingsObserver extends ContentObserver {
        SettingsObserver(Handler handler) {
            super(handler);
        }

        void observe() {
            // Observe all users' changes
            ContentResolver resolver = mContext.getContentResolver();
            resolver.registerContentObserver(
                    Settings.System.getUriFor(Settings.System.PROFILER_SERVICE),
                    false, this, UserHandle.USER_ALL);
            updateSettings();
        }

        @Override
        public void onChange(boolean selfChange) {
            updateSettings();
        }
    }

    private static final int MSG_ENABLE_PROFILER = 1;
    private static final int MSG_DISABLE_PROFILER = 2;

    private class InputHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_ENABLE_PROFILER:
                    enableProfilerInput();
                    break;
                case MSG_DISABLE_PROFILER:
                    disableProfilerInput();
                    break;
            }
        }
    }

    @Override
    public void monitor() {
        synchronized (mLock) {

        }
    }
}
